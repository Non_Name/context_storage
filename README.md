# context_storage

## Platform

- Linux Ubuntu 22.04

## Task points

Only 2/4 points of the task are completed (More flexible variant).

## Features:

- Gitlab CI;
- Using git;
- Process exceptions for type comparison;
- Dynamic lib from context.hpp with work example.
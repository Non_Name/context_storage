#include <iostream>
#include <vector>
#include <map>
#include <cstring>
#include "context.hpp"
#include "search_type.hpp"

template<typename T>
bool waitGetException(Context & context)
{
    try
    {
        T result = context.get<T>();
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
        return true;
    }
    return false;
}

template<typename T>
bool waitSetException(Context & context, T value)
{
    try
    {
        context.set(std::move(value));
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
        return true;
    }
    return false;
}

template<typename T, typename U>
void searchTest(U & data, bool flag = true)
{
    if(SearchType::findElement<T>(data) == flag)
    {
        std::cout << "Search: OK" << "\n\n";
    }
    else
    {
        std::cout << "Search: Error" << "\n\n";
    }
}

int main(int argc, char **argv)
{
    std::cout << "Test Case 1" << "\n";
    Context test_case1(10);
    if(waitGetException<char>(test_case1))
    {
        std::cout << "OK" << "\n";
    }    
    if(!waitSetException(test_case1, 12))
    {
        std::cout << "OK" << "\n";
    }
    std::cout << test_case1.get<int>() << "\n\n";

    std::cout << "Test Case 2" << "\n";
    Context test_case2(std::move(test_case1));
    auto type1 = test_case1.getType();
    if(type1 == nullptr)
    {
        std::cout << "OK" << "\n\n";
    }

    std::cout << "Test Case 3" << "\n";
    Context test_case3(std::vector<Context>{Context(10), Context(std::string("al"))});
    auto& temp1 = test_case3.get<std::vector<Context>>();
    temp1.push_back(Context(5));
    std::map<std::string, Context> map1{{"key", Context(10)}};
    temp1.push_back(Context(map1));
    auto temp2 = test_case3.get<std::vector<Context>>();
    std::cout << "Size: " << temp2.size() << "\n\n";

    std::cout << "Test Case 4" << "\n";
    searchTest<int>(test_case3, true);  
    searchTest<std::map<std::string, Context>>(test_case3, true); 
    searchTest<char>(test_case3, false);

    auto& temp3 = temp2[3].get<std::map<std::string, Context>>();
    temp3.insert(std::pair<std::string, Context>{"key2", Context('f')});
    searchTest<char>(test_case3, true);
    return 0;
}
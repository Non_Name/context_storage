cmake_minimum_required(VERSION 3.14)
project(context_storage VERSION 1.0.0 LANGUAGES CXX)


include_directories(${CMAKE_SOURCE_DIR}/include)
add_subdirectory(${CMAKE_SOURCE_DIR}/include)

add_executable(${PROJECT_NAME} ${CMAKE_SOURCE_DIR}/main.cpp)

target_link_libraries(
    ${PROJECT_NAME}
    PRIVATE
    context
)

set_target_properties(
    ${PROJECT_NAME}
    PROPERTIES
    CMAKE_CXX_STANDARD 17
)
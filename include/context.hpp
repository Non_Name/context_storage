#ifndef CONTEXT_HPP
#define CONTEXT_HPP
#include <typeinfo>
#include <vector>
#include <memory>

struct DataHolder
{
    std::shared_ptr<void> data;
    const std::type_info *type;
};

class Context
{
public:
    Context();
    template<typename T>
    Context(T & data);
    template<typename T>
    Context(T && data);
    Context(const Context & context);
    Context(Context && context);
    Context& operator=(const Context& context);
    Context& operator=(Context && context);
    template<typename T>
    T & get();
    template<typename T>
    void set(T & data);
    template<typename T>
    void set(T && data);
    const std::type_info* getType();
    DataHolder& getDataHolder();
private:
    DataHolder m_data;    
};


Context::Context()
{
    m_data.type = nullptr;
    m_data.data = nullptr;
}

Context::Context(const Context & context)
{
    m_data.data = context.m_data.data;
    m_data.type = context.m_data.type;
}

Context::Context(Context && context)
{
    m_data.data = std::move(context.m_data.data);
    m_data.type = context.m_data.type;
    context.m_data.type = nullptr;
    context.m_data.data = nullptr;
}

Context& Context::operator=(const Context & context)
{
    if(this == &context)
    {
        return *this;
    }
    m_data.data = context.m_data.data;
    m_data.type = context.m_data.type;
    return *this;
}

Context& Context::operator=(Context && context)
{
    if(this == &context)
    {
        return *this;
    }
    m_data.data = context.m_data.data;
    m_data.type = context.m_data.type;
    context.m_data.data = nullptr;
    context.m_data.type = nullptr;
    return *this;
}

template<typename T>
Context::Context(T & data)
{
    m_data.type = &typeid(T);
    m_data.data = std::make_shared<T>(data);
}

template<typename T>
Context::Context(T && data)
{
    m_data.type = &typeid(T);
    m_data.data = std::make_shared<T>(std::forward<T>(data));
    data = T{};
}

template<typename T>
T & Context::get()
{
    if(typeid(T).hash_code() != m_data.type->hash_code())
    {
        throw std::logic_error("Incorrect type");
    }
    return *reinterpret_cast<T*>(m_data.data.get());
}

template<typename T>
void Context::set(T & data)
{
    if(m_data.type == nullptr)
    {
        m_data.type = &typeid(T);
        m_data.data = std::make_shared<T>(data);
        return;  
    }
    else if(typeid(T).hash_code() != m_data.type->hash_code())
    {
        throw std::logic_error("Incorrect type");
    }
    *reinterpret_cast<T*>(m_data.data.get()) = data;
}

template<typename T>
void Context::set(T && data)
{
    if(m_data.type == nullptr)
    {
        std::cout << "Checkpoint" << "\n";
        m_data.type = &typeid(T);
        m_data.data = std::make_shared<T>(std::forward<T>(data));
        return;  
    }
    else if(typeid(T).hash_code() != m_data.type->hash_code())
    {
        std::cout << "Checkpoint" << "\n";
        throw std::logic_error("Incorrect type");
    }
    *reinterpret_cast<T*>(m_data.data.get()) = std::forward<T>(data);
}

const std::type_info* Context::getType()
{
    return m_data.type;
}

DataHolder& Context::getDataHolder()
{
    return m_data;
}

#endif      // CONTEXT_HPP
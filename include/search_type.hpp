#ifndef SEARCH_TYPE_HPP
#define SEARCH_TYPE_HPP
#include <map>
#include "context.hpp"

//search type + move hierarchy, type comparator

class SearchType
{
public:
    template<typename T>
    static bool compareTypes(const std::type_info * info);
    template<typename T>
    static bool findElement(DataHolder & data);
    template<typename T>
    static bool findElement(Context & context);
    template<typename T>
    static bool findElement(std::vector<Context> & context);
    template<typename T>
    static bool findElement(std::map<std::string, Context> & context);
    template<typename T>
    static bool detectType(Context & context); 
};

template<typename T>
bool SearchType::compareTypes(const std::type_info * object_val)
{
    try
    {
        const std::type_info& selected_val = typeid(T);
        if(selected_val.hash_code() != object_val->hash_code())
        {
            throw std::logic_error("Incorrect type");
        }
    }
    catch(const std::exception& e)
    {
        std::cerr << "ERROR: " << e.what() << '\n';
        return false;
    }
    return true;
}

template<typename T>
bool SearchType::findElement(std::vector<Context> & context)
{
    const std::type_info* object_val = &typeid(std::vector<Context>);
    bool result{false};
    std::cout << "Vector" << "\n";
    if(compareTypes<T>(object_val))
    {
        result = true;
    }
    else
    {
        for(auto & i : context)
        {
            result = findElement<T>(i);
            if(result)
            {
                break;      
            }            
        }
    }
    return result;
}

template<typename T>
bool SearchType::findElement(std::map<std::string, Context> & context)
{
    const std::type_info* object_val = &typeid(std::map<std::string, Context>);
    bool result{false};
    std::cout << "Map" << "\n";
    if(compareTypes<T>(object_val))
    {
        result = true;
    }
    else
    {
        for(auto & key : context)
        {
            result = findElement<T>(key.second);
            if(result)
            {
                break;     
            }            
        }
    }
    return result;
}

template<typename T>
bool SearchType::detectType(Context & context)
{
    const std::type_info* object_val = context.getType(); //from m_data(DataHolder)
    bool result{false};
    std::cout << "Type detection" << "\n";
    try
    {
        if(object_val == nullptr)
        {
            std::logic_error("Element doesn't exist");
        }
        if(compareTypes<Context>(object_val))
        {
            Context child_context = std::forward<Context>(context.get<Context>());
            result = findElement<T>(child_context);
        }
        else if(compareTypes<std::vector<Context>>(object_val))
        {
            std::vector<Context> child_context = context.get<std::vector<Context>>();
            result = findElement<T>(child_context);
        }
        else if(compareTypes<std::map<std::string, Context>>(object_val))
        {
            std::map<std::string, Context> child_context = context.get<std::map<std::string, Context>>();
            result = findElement<T>(child_context);
        }
        else
        {
            DataHolder data_holder = context.getDataHolder();
            result = findElement<T>(data_holder);
        }
    }
    catch(const std::exception& e)
    {
        std::cerr << "ERROR: " << e.what() << "\n";
        result = false; 
    }
    return result;
}

template<typename T>
bool SearchType::findElement(Context & context)
{
    std::cout << "Context" << "\n";
    const std::type_info* object_val = &typeid(Context);
    if(compareTypes<T>(object_val))
    {
        return true;
    }
    return detectType<T>(context);
}

template<typename T>
bool SearchType::findElement(DataHolder & data)
{
    std::cout << "Value" << "\n";
    if(!compareTypes<T>(data.type) || data.data == nullptr)
    {
       throw std::logic_error("Element not found");
    }
    bool result = true;
    return result;
}

#endif      // SEARCH_TYPE_HPP